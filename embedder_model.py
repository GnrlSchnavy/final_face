import tensorflow as tf
import align.detect_face
import facenet
import numpy as np
from scipy import misc
import os

cwd = os.getcwd()  # working directory


class embedding_model:
    def __init__(self):
        with tf.Session() as self.sess:
            self.image_size = 160
            self.margin = 44
            self.sess = tf.Session()
            self.minsize = 20  # minimum size of face
            self.threshold = [0.6, 0.7, 0.7]  # three steps's threshold
            self.factor = 0.709  # scale factor
            print('Creating networks and loading parameters')
            self.pnet, self.rnet, self.onet = align.detect_face.create_mtcnn(self.sess, None)
            model = cwd + "/modelx/20170512-110547.pb"
            # model = cwd + "/modelx/20180402-14759/20180402-114759.pb"
            # Load the model
            print("start load_model")
            facenet.load_model(model)
            print("end load_model")

            # Get input and output tensors
            self.images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
            self.embeddings = tf.get_default_graph().get_tensor_by_name("embeddings:0")
            self.phase_train_placeholder = tf.get_default_graph().get_tensor_by_name("phase_train:0")

    def run_session(self, images):
        feed_dict = {self.images_placeholder: images, self.phase_train_placeholder: False}
        emb = self.sess.run(self.embeddings, feed_dict=feed_dict)
        return emb

    def get_multiple_embeddings(self, image_paths):
        tmp_image_paths = image_paths.copy()
        img_list = []
        for image in tmp_image_paths:
            img = misc.imread(os.path.expanduser(image), mode='RGB')
            img_size = np.asarray(img.shape)[0:2]
            bounding_boxes, _ = align.detect_face.detect_face(img, self.minsize, self.pnet, self.rnet, self.onet,
                                                              self.threshold, self.factor)
            det = np.squeeze(bounding_boxes[0, 0:4])
            bb = np.zeros(4, dtype=np.int32)
            bb[0] = np.maximum(det[0] - self.margin / 2, 0)
            bb[1] = np.maximum(det[1] - self.margin / 2, 0)
            bb[2] = np.minimum(det[2] + self.margin / 2, img_size[1])
            bb[3] = np.minimum(det[3] + self.margin / 2, img_size[0])
            cropped = img[bb[1]:bb[3], bb[0]:bb[2], :]
            aligned = misc.imresize(cropped, (self.image_size, self.image_size), interp='bilinear')
            prewhitened = facenet.prewhiten(aligned)
            img_list.append(prewhitened)
        images = np.stack(img_list)
        return images

    def get_single_image_embedding(self, img):
        img_list = []
        img_size = np.asarray(img.shape)[0:2]
        bounding_boxes, _ = align.detect_face.detect_face(img, self.minsize, self.pnet, self.rnet, self.onet,
                                                          self.threshold, self.factor)
        det = np.squeeze(bounding_boxes[0, 0:4])
        bb = np.zeros(4, dtype=np.int32)
        bb[0] = np.maximum(det[0] - self.margin / 2, 0)
        bb[1] = np.maximum(det[1] - self.margin / 2, 0)
        bb[2] = np.minimum(det[2] + self.margin / 2, img_size[1])
        bb[3] = np.minimum(det[3] + self.margin / 2, img_size[0])
        cropped = img[bb[1]:bb[3], bb[0]:bb[2], :]
        aligned = misc.imresize(cropped, (self.image_size, self.image_size), interp='bilinear')
        prewhitened = facenet.prewhiten(aligned)
        img_list.append(prewhitened)
        images = np.stack(img_list)
        return images
