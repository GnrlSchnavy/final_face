from collections import OrderedDict
import detect_faces_video
import os
import embedder_model
import numpy as np
import pyttsx3
from datetime import datetime
from slackclient import SlackClient

slack_token = "xoxp-318155828310-317206728195-351408853890-5fdd6cd9c0d68ebc13b1e76ccd2b4e37"
sc = SlackClient(slack_token)
people_timer = {}  # dict to keep of already recognized persons
engine = pyttsx3.init()
cwd = os.getcwd()  # working directory
threshold = 0.3  # min value that the Eucledian distance should be before a person is identified as the same person
face_detection_sensitivity = 0.85  # value that determines when a face,in general is detected
forget_time = 60  # time in seconds before the systems recognizes a person again (should be lowered for testing)


def get_images(path):
    img_dir = os.path.dirname(os.path.realpath(__file__)) + "/sioux_images/"
    tmp = []
    for file in os.listdir(path):
        tmp.append(img_dir + file)
    return tmp


images = get_images(cwd + "/sioux_images")
images.insert(0, "placeholder")

def welcome_message(name):
    engine.say("Welcome to Seeoox " + name + "!!")
    engine.runAndWait()
    print("Welcome to seeyouxx " + name + " !!")
    people_timer[name] = datetime.now()
    sc.api_call(
        "chat.postMessage",
        channel="bot",
        text="Reckognized " + name
    )

def main(model):
    print("Loading face detection model")
    face_detection_model = cwd + "/modelx/default.caffemodel"
    prototext = cwd + "/modelx/deploy.prototxt"
    print("Getting database images embeddings")
    images = get_images(cwd + "/sioux_images")
    database_embedded = model.run_session(model.get_multiple_embeddings(images))
    detect_faces_video.main(face_detection_model, prototext, face_detection_sensitivity, model, database_embedded)


def compare_images(imencode, model, database_embedded):
    embedded_image = model.run_session(model.get_single_image_embedding(imencode))
    database_embedded = (np.concatenate((embedded_image, database_embedded), axis=0))
    best_guess = [0, 0, "", 2]
    distance_overview = {}
    for i in range(len(database_embedded)):
        dist = np.sqrt(np.sum(np.square(np.subtract(database_embedded[0, :], database_embedded[i, :]))))
        distance_overview[os.path.basename(images[i])] = dist
        if dist < best_guess[3] and dist != 0:
            best_guess[0] = i
            best_guess[2] = images[i]
            best_guess[3] = dist
        if best_guess[3] < 0.7:  # if a value is below 0.7 the system is already VERY sure this is the correct person
            continue

    if get_closest(distance_overview):  # also person not in timer
        name = (str(os.path.basename(best_guess[2]).split('_')[0]))
        if name in people_timer:
            # print("check time update time")
            if abs((people_timer.get(name) - datetime.now()).total_seconds()) > forget_time:
                welcome_message(name)
            else:
                a=1
                # print("Already detected this person in last ", forget_time, " seconds")

        else:
            # print("add to people timer")
            people_timer[name] = datetime.now()
            welcome_message(name)
    else:
        print(best_guess)
        print("Sorry I couldn't recognize you!")


def get_closest(distance_overview):
    sorted_dict = OrderedDict(sorted(distance_overview.items(), key=lambda t: t[1]))
    sorted_dict.popitem(last=False)  # remove placeholder
    best_guess = sorted_dict.popitem(last=False)
    print(best_guess)
    deduction_best_guess = 0
    closest_three_avg = 0
    counter = 0
    while counter < 3:
        current = sorted_dict.popitem(last=False)
        if current[0][:4] in best_guess[0]:
            continue
        else:
            closest_three_avg += (1 / 3) * current[1]
            counter += 1
    # print(closest_three_avg - best_guess[1])
    if closest_three_avg > best_guess[1] + threshold + deduction_best_guess:
        return True
    else:
        return False


if __name__ == '__main__':
    main(embedder_model.embedding_model())
