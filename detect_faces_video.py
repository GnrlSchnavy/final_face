from imutils.video import VideoStream
import numpy as np
import imutils
import time
import cv2
import controller


def main(face_detection_model, prototext, face_detection_sensitivity, model, database_embedded):
    net = cv2.dnn.readNetFromCaffe(prototext, face_detection_model)
    vs = VideoStream(src=0).start()
    time.sleep(2.0)
    process_this_frame = 0

    while True:
        frame = vs.read()
        frame = imutils.resize(frame, width=256)
        if process_this_frame % 3 == 0:
            process_this_frame = 1
            (h, w) = frame.shape[:2]
            blob = cv2.dnn.blobFromImage(cv2.resize(frame, (128, 128)), 1.0, (128, 128), (104.0, 177.0, 123.0))
            net.setInput(blob)
            detections = net.forward()
            for i in range(0, detections.shape[2]):
                confidence = detections[0, 0, i, 2]
                if confidence < face_detection_sensitivity:
                    continue
                else:
                    box = compute_draw_boundingbox(detections, i, w, h, frame)
                    cropped_image = frame[box[0]:box[1], box[2]:box[3]]
                    try:
                        controller.compare_images(cropped_image, model, database_embedded)
                        cv2.imshow("Frame", frame)
                    except:
                        continue
        process_this_frame += 1
        cv2.imshow("Frame", frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break


def compute_draw_boundingbox(detections, i, w, h, frame):
    box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
    (startX, startY, endX, endY) = box.astype("int")
    return [startY, endY, startX, endX]


if __name__ == '__main__':
    main()
